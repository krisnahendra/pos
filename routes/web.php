<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\AdminController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', [AuthController::class, 'index']);
Route::get('/login', [AuthController::class, 'index']);
Route::get('/register', [AuthController::class, 'register']);
Route::get('/logout', [AuthController::class, 'logout']);

Route::post('/login', [AuthController::class, 'checkLogin']);
Route::post('/register', [AuthController::class, 'postRegister']);

// Admin
Route::get('/admin', [AdminController::class, 'index']);

Route::get('/user', [AdminController::class, 'user']);
Route::post('/user', [AdminController::class, 'addUser']);
Route::post('/deleteUser', [AdminController::class, 'deleteUser']);
Route::post('/editUser', [AdminController::class, 'editUser']);

Route::get('/barang', [AdminController::class, 'barang']);
Route::post('/barang', [AdminController::class, 'tambahBarang']);
Route::post('/deleteBarang', [AdminController::class, 'deleteBarang']);
Route::post('/editBarang', [AdminController::class, 'editBarang']);
Route::get('/cariBarang', [AdminController::class, 'cariBarang']);

Route::get('/pelanggan', [AdminController::class, 'pelanggan']);
Route::post('/pelanggan', [AdminController::class, 'tambahPelanggan']);
Route::post('/deletePelanggan', [AdminController::class, 'deletePelanggan']);
Route::post('/editPelanggan', [AdminController::class, 'editPelanggan']);

Route::get('/transaksi', [AdminController::class, 'transaksi']);
Route::get('/jenisTransaksi', [AdminController::class, 'jenisTransaksi']);
Route::get('/addToCart',[AdminController::class, 'addToCart']);
Route::get('/clearCart', [AdminController::class, 'clearCart']);
Route::get('/deleteCart', [AdminController::class, 'deleteCart']);
Route::post('/submitCart', [AdminController::class, 'submitCart']);

Route::get('/transaksiBarcode', [AdminController::class, 'transaksiBarcode']);
Route::post('/addToCartBarcode', [AdminController::class, 'addToCartBarcode']);
Route::get('/jenisTransaksiBarcode', [AdminController::class, 'jenisTransaksiBarcode']);
Route::get('/clearCartBarcode', [AdminController::class, 'clearCartBarcode']);
Route::get('/deleteCartBarcode', [AdminController::class, 'deleteCartBarcode']);
Route::post('/submitCartBarcode', [AdminController::class, 'submitCartBarcode']);

Route::get('/riwayatTransaksi', [AdminController::class, 'riwayatTransaksi']);
Route::get('/printStruk', [AdminController::class, 'printStruk']);
