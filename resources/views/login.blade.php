<!DOCTYPE html>
<html lang="en">
<head>
	<title>Login V4</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="{{ url('assets/login') }}/images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ url('assets/login') }}/vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ url('assets/login') }}/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ url('assets/login') }}/fonts/iconic/css/material-design-iconic-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ url('assets/login') }}/vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="{{ url('assets/login') }}/vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ url('assets/login') }}/vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ url('assets/login') }}/vendor/select2/select2.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="{{ url('assets/login') }}/vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ url('assets/login') }}/css/util.css">
	<link rel="stylesheet" type="text/css" href="{{ url('assets/login') }}/css/main.css">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
<!--===============================================================================================-->
    <style>
        .border-0{
            border-radius: 0px !important;
        }
        .bg-custom{
            background: #f5f5f5 !important;
        }
        .border-wrap-login{
            border: 1px solid #e5e5e5 !important;
            box-shadow: 0px 10px 34px -15px rgb(0 0 0 / 24%) !important;
        }
        .swal2-confirm{
            background: red !important;
        }
    </style>
</head>
<body>
	
	<div class="limiter">
		<div class="container-login100 bg-custom">
			<div class="wrap-login100 p-l-55 p-r-55 p-t-50 p-b-50 border-0 border-wrap-login">
				<form id="formLogin" class="login100-form validate-form" action="{{ url('login') }}" method="POST">
                    @csrf
                    <div class="row">
                        <div class="col text-center">
                            {{-- <img src="https://icons-for-free.com/iconfiles/png/512/locked+login+password+privacy+private+protect+protection+safe-1320196167397530530.png" alt="..." width="60px" class="mb-3"> <br> --}}
                            <small class="font-weight-bold text-secondary">SISTEM INFORMASI</small>
                            <span class="login100-form-title p-b-25 text-primary">
                                Cahaya Titan
                            </span>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col">
                            <div class="alert alert-danger alert-dismissible text-center fade show" role="alert" id="alertError" style="display: none;">
                                <strong><i class="fa fa-times-circle"></i> </strong> <span id="errorMessage"></span>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>
                    </div>

					<div class="wrap-input100 validate-input m-b-23">
						<span class="label-input100">Username</span>
						<input class="input100" type="text" id="username" name="username" placeholder="Masukkan username">
						<span class="focus-input100" data-symbol="&#xf206;"></span>
					</div>

					<div class="wrap-input100 validate-input">
						<span class="label-input100">Password</span>
						<input class="input100" type="password" id="password" name="password" placeholder="Masukkan password">
						<span class="focus-input100" data-symbol="&#xf190;"></span>
					</div>
					
					<div class="text-right p-t-8 p-b-31">
						<a href="#">
							Forgot password?
						</a>
					</div>
					
					<div class="container-login100-form-btn">
						<div class="wrap-login100-form-btn border-0">
							<div class="login100-form-bgbtn"></div>
							<button type="submit" class="login100-form-btn border-0 btn-primary" id="btnLogin">
								Login
							</button>
						</div>
					</div>

					

					

					
				</form>
			</div>
		</div>
	</div>
	

	<div id="dropDownSelect1"></div>
	
<!--===============================================================================================-->
	<script src="{{ url('assets/login') }}/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="{{ url('assets/login') }}/vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="{{ url('assets/login') }}/vendor/bootstrap/js/popper.js"></script>
	<script src="{{ url('assets/login') }}/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="{{ url('assets/login') }}/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="{{ url('assets/login') }}/vendor/daterangepicker/moment.min.js"></script>
	<script src="{{ url('assets/login') }}/vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="{{ url('assets/login') }}/vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
    <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
	<script src="{{ url('assets/login') }}/js/main.js"></script>

    @if (Session::has('message'))
        <script>
            toastr.success('You have been logged out!', 'Success');
        </script>
    @endif

</body>
</html>