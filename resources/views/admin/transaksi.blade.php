@extends('template')

@section('content')

<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">

<style>
  .selected{
    color: red;
  }
 .navbar-form
{
    position: relative;
}
.form-control
{
    width: 100%;
    margin-bottom: 7px;
}
#searchResults
{
    position: absolute;
    width: auto;
    background: white;
    /* border-bottom-left-radius: 10px;
    border-bottom-right-radius: 10px; */
    max-height: 200px;
    overflow-y: auto;
    overflow-x: hidden;

    border: 1px solid #e5e5e5;

    /*This is relative to the navbar now*/
    left: 41px;
    right: 0;
    top: 45px;
    z-index: 100;
    /* padding: 8px; */
    display: none;
}
.col-result:hover{
  background: #e5e5e5;
  cursor: pointer;
}
.resultData{
  list-style: none;
  padding: 0;
  margin: 0;
}
.li-result{
  padding: 5px 10px;
  cursor: pointer;
  border-bottom: 1px solid #DCDFE8;
}
.li-result:hover{
  background: #e5e5e5;
}
.border-0{
  border-radius: 0;
}
.border-0-form{
  border-radius: 0;
  border: 1px solid #DCDFE8;
}
.table-cart td{
  padding: 6px 7px !important;
}
td .form-control{
  margin-bottom: 0;
}
.v-align-middle{
  vertical-align: middle !important;
}
</style>

<div class="content-page">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-8">
        <div class="card border shadow">
          <div class="card-header font-weight-bold py-2" style="border-top-left-radius: 10px; border-top-right-radius: 10px;">
            <div class="row">
              <div class="col-lg-6 my-auto">
                <i class="fa fa-shopping-cart"></i> TRANSAKSI <small>{{ date('d F Y') }}</small>
              </div>
              <div class="col-lg-6 text-right">
                <div class="btn-group" role="group" aria-label="Basic example">
                  @if($jenis == 'do')
                  <a href="{{ url('jenisTransaksi?jenis=biasa') }}" class="border-0 btn btn-light font-weight-bold">Biasa</a>
                  <a href="{{ url('jenisTransaksi?jenis=do') }}" class="border-0 btn btn-primary font-weight-bold">DO</a>
                  @else
                  <a href="{{ url('jenisTransaksi?jenis=biasa') }}" class="border-0 btn btn-primary font-weight-bold">Biasa</a>
                  <a href="{{ url('jenisTransaksi?jenis=do') }}" class="border-0 btn btn-light font-weight-bold">DO</a>
                  @endif
                </div>
              </div>
            </div>
          </div>
          <div class="card-body">
            <div class="row">
              <div class="col-12">
                {{-- <div class="form-group">
                  <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Cari atau scan barcode disini ..." autofocus>
                </div> --}}
                <div class="navbar-form navbar-left">
                  <div class="input-group mb-3">
                    <div class="input-group-prepend">
                      <span class="input-group-text" id="basic-addon1" style="border-top-left-radius: 0; border-bottom-left-radius: 0;"><i class="fa fa-search m-0"></i> </span>
                    </div>
                    <input class="form-control" placeholder="Cari nama barang ..." type="text" autocomplete="off"  id="inputSearch" style="border-radius: 0;" autofocus/>
                  </div>
                  <div id="searchResults">
                  </div>
                </div>
              </div>
            </div>

            @php
              $grandTotal = 0;
              foreach($cart as $c){
                $grandTotal += $c->subtotal;
              }
            @endphp

            <form action="{{ url('submitCart') }}" method="POST">
            @csrf
            @if($jenis == 'biasa')
            <input type="hidden" name="kode_transaksi" value="INV{{ time() }}">
            @else
            <input type="hidden" name="kode_transaksi" value="DO{{ time() }}">
            @endif
            <input type="hidden" name="grand_total" value="{{ $grandTotal }}">
            <input type="hidden" name="jenisTransaksi" value="{{ $jenis }}">
            <div class="row mb-2">
              <div class="col-6">
                <a href="{{ url('clearCart?jenis='.$jenis) }}" class="btn btn-red border-0"><i class="fa fa-times-circle"></i> Kosongkan Cart</a>
              </div>
              <div class="col-6 text-right">
                <input type="submit" name="update" value="Update" class="btn btn-success border-0">
                <input type="submit" name="print" value="Print" class="btn btn-primary border-0">
              </div>
            </div>

            <div class="row">
              <div class="col-12">
                <table class="table table-sm table-cart" style="border: 1px solid rgba(0, 0, 0, 0.1) !important; border-radius: 0 !important;">
                  <thead class="bg-info font-weight-bold text-white">
                    <tr>
                      <td class="text-center v-align-middle"><i class="fa fa-trash-alt"></i></td>
                      <td class="text-center v-align-middle">NAMA</td>
                      <td class="text-center v-align-middle">HARGA</td>
                      <td class="text-center v-align-middle">JUMLAH</td>
                      <td class="text-center v-align-middle">SUBTOTAL</td>
                    </tr>
                  </thead>
                  <tbody>
                    @if(count($cart) != 0)
                    @foreach($cart as $c)
                    <tr>
                      <td class="text-center v-align-middle">
                        <a href="{{ url('deleteCart?id_cart='.$c->id.'&jenis='.$jenis.'') }}" class="btn btn-sm btn-red text-center border-0"><i class="fa fa-trash-alt m-0"></i></a>
                      </td>
                      <td class="text-center v-align-middle">
                        <input type="text" class="form-control form-control-sm text-center border-0-form font-weight-bold" value="{{ $c->nama_barang }} ({{ $c->keterangan }})" readonly style="width: 220px;">
                      </td>
                      <td class="text-center v-align-middle">
                        <input type="text" class="form-control form-control-sm text-right border-0-form font-weight-bold" value="{{ number_format($c->harga) }}" readonly>
                      </td>
                      <td class="text-center v-align-middle">
                        <input type="hidden" name="id_cart[]" value="{{ $c->id }}">
                        <input type="number" class="form-control form-control-sm text-center border-0-form" name="jumlah[]" value="{{ $c->jumlah }}" style="width: 80px;">
                      </td>
                      <td class="text-center v-align-middle">
                        <input type="text" class="form-control form-control-sm text-right border-0-form font-weight-bold" value="{{ number_format($c->subtotal) }}" readonly>
                      </td>
                    </tr>
                    @endforeach
                    @else
                    <tr>
                      <td colspan="5" class="text-center">
                        <div class="alert alert-danger mb-0" role="alert">
                          <div class="iq-alert-text">
                            Belum ada barang yang ditambahkan.
                          </div>
                        </div>
                      </td>
                    </tr>
                    @endif
                  </tbody>
                </table>
              </div>
            </div>

          </div>
        </div>
      </div>
      <div class="col-lg-4">
        <div class="card border shadow mb-2">
          <div class="card-header font-weight-bold py-2" style="border-top-left-radius: 10px; border-top-right-radius: 10px;">
            <div class="row">
              <div class="col-10">
                <i class="fa fa-sticky-note"></i> INFORMASI NOTA
              </div>
              <div class="col-2 text-right">
                <a href="#" class="btn btn-sm btn-white text-white">-</a>
              </div>
            </div>
          </div>
          <div class="card-body">

            <div class="alert alert-primary border-0" role="alert" style="border-left: 4px solid #00c4ff !important;">
              <div class="iq-alert-text text-center">
                <u>Grand Total :</u> <br>
                <h3 class="m-0">
                  Rp{{ number_format($grandTotal) }}
                </h3>
              </div>
           </div>
            <table style="width: 100%">
              <tr>
                <td class="font-weight-bold mb-1">No. Nota</td>
                <td>:</td>
                <td>
                  @if($jenis == 'biasa')
                    <input type="text" class="form-control form-control-sm border-0-form mb-1" value="INV{{time()}}" readonly>
                  @else
                    <input type="text" class="form-control form-control-sm border-0-form mb-1" value="DO{{time()}}" readonly>
                  @endif
                </td>
              </tr>
              <tr>
                <td class="font-weight-bold mb-1">Tanggal</td>
                <td>:</td>
                <td>
                  <input type="text" class="form-control form-control-sm border-0-form mb-1" value="{{ date('d M Y') }}" readonly>
                </td>
              </tr>
              <tr>
                <td class="font-weight-bold mb-1">Kasir</td>
                <td>:</td>
                <td>
                  <input type="text" class="form-control form-control-sm border-0-form mb-1" name="kasir" value="{{ $user['nama'] }}" readonly>
                </td>
              </tr>
              <tr>
                <td class="font-weight-bold mb-1">Pelanggan</td>
                <td>:</td>
                <td>
                  <input type="text" class="form-control form-control-sm border-0-form mb-1" name="pelanggan" value="UMUM">
                </td>
              </tr>
              <tr>
                <td class="font-weight-bold mb-1 text-primary">Catatan Struk</td>
                <td>:</td>
                <td>
                  <input type="text" class="form-control form-control-sm border-0-form mb-1" name="catatan" value="-">
                </td>
              </tr>
            </table>

          </form>
          </div>
        </div>


        <div class="card border shadow">
          <div class="card-body text-center">
            <hr class="my-1">
            <b class="text-uppercase text-red">Tombol Shortcut</b>
            <hr class="my-1">
            <div class="row">
              <div class="col">
                <table style="width:100%">
                  <tr>
                    <td class="font-weight-bold text-primary">F2</td>
                    <td><i class="fa fa-arrow-right"></i></td>
                    <td>Hapus</td>
                  </tr>
                  <tr>
                    <td class="font-weight-bold text-primary">F2</td>
                    <td><i class="fa fa-arrow-right"></i></td>
                    <td>Hapus</td>
                  </tr>
                  <tr>
                    <td class="font-weight-bold text-primary">F2</td>
                    <td><i class="fa fa-arrow-right"></i></td>
                    <td>Hapus</td>
                  </tr>
                </table>
              </div>
              <div class="col" style="border-left: 1px solid rgba(0, 0, 0, 0.1);">
                <table style="width:100%">
                  <tr>
                    <td class="font-weight-bold text-primary">F2</td>
                    <td><i class="fa fa-arrow-right"></i></td>
                    <td>Hapus</td>
                  </tr>
                  <tr>
                    <td class="font-weight-bold text-primary">F2</td>
                    <td><i class="fa fa-arrow-right"></i></td>
                    <td>Hapus</td>
                  </tr>
                  <tr>
                    <td class="font-weight-bold text-primary">F2</td>
                    <td><i class="fa fa-arrow-right"></i></td>
                    <td>Hapus</td>
                  </tr>
                </table>
              </div>
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>
</div>

<script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>

<script>
  @if(Session::has('barangSudahAda'))
    Swal.fire({
      icon: 'warning',
      title: 'Gagal',
      text: 'Barang sudah ditambahkan dalam keranjang!',
      showConfirmButton: false,
      timer: 2000
    })
    // toastr.error('Barang sudah ditambahkan dalam keranjang!','GAGAL')
  @endif

  @if(Session::has('stokHabis'))
    Swal.fire({
      icon: 'error',
      title: 'Gagal',
      text: 'Stok barang tidak mencukupi',
      showConfirmButton: false,
      timer: 2000
    })
  @endif
</script>

<script>
  $(function () {
    $(document).keyup(function (e) {
        let inputSearch = $('#inputSearch');
        let searchResults = $('#searchResults');
        if(e.keyCode == 113){
          searchResults.hide();
          inputSearch.val('');
        }
    });
  });
</script>


<script>
  $('#inputSearch').keyup(function(e){
    var inputValue = $('#inputSearch').val()
    console.log(inputValue)
    var searchResults = $('#searchResults')
    var jenisTransaksi = "{{ $jenis }}"
    var next = 'ok';

    if(e.keyCode == 113){
      next = 'no';
      searchResults.hide()
      $('#searchResults').val('')
    }
    console.log(inputValue)

    if(next == 'ok'){
      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });
      $.ajax({
          type: "GET",
          url: "{{ url('cariBarang') }}"+"?inputValue="+inputValue+"&jenisTransaksi="+jenisTransaksi,
          data: {inputValue, jenisTransaksi},
          dataType:'json',
          processData: false,
          contentType: false,
          success: function(data){
            console.log(data.data.length);
            if(data.data.length != 0){
              searchResults.show()
              searchResults.html('<ul class="resultData">'+data.data+'</ul>')
            }else{
              searchResults.hide()
            }
          }
      })
    }else{
      searchResults.hide();
    }
});

</script>


{{-- <script>
var count = 0;
$("#inputSearch")
  .focusout(function() {
    count++
  })
  .blur(function() {
    $('#searchResults').hide();
    $('#inputSearch').val('');
  });
</script> --}}

@endsection
