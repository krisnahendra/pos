@extends('template')

@section('content')

<div class="content-page">
    <div class="container-fluid">
       <div class="row">
           <div class="col-lg-12">
               <div class="d-flex flex-wrap align-items-center justify-content-between mb-4">
                   <div>
                       <h4 class="mb-3">List Data Barang</h4>
                       <p class="mb-0">Dibawah ini merupakan list data barang hingga tanggal {{date('d F Y') }} <br> dan anda bisa melakukan tambah, edit, dan hapus data barang.</p>
                   </div>
                   <a href="#modalTambahBarang" data-toggle="modal" class="btn btn-primary add-list"><i class="las la-plus mr-2"></i>Tambah Barang</a>
               </div>
           </div>
           <div class="col-lg-12">
               <div class="row">
                   <div class="col-lg-12">
                    <div class="card border shadow">
                        <div class="card-header py-3 font-weight-bold">
                            <div class="row">
                                <div class="col-lg-4 text-uppercase my-auto text-primary-dark">
                                    <i class="las la-box"></i> Data Barang {{ date('d M Y') }}
                                </div>
                                <div class="col-lg-8 text-right">
                                    <a href="" class="btn btn-md btn-primary-dark">
                                        <i class="las la-print"></i> Print
                                    </a>
                                    <a href="" class="btn btn-md btn-warning">
                                        <i class="las la-file-download"></i> PDF
                                    </a>
                                    <a href="" class="btn btn-md btn-success">
                                        <i class="las la-file-excel"></i> Excel
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="card shadow" style="height: 104px;">
                                        <div class="card-body text-center">
                                                <h5 class="text-primary font-weight-bold" style="text-decoration: underline;">Total Barang</h5>
                                                <h3 class="m-0">{{ $infoBarang['total_barang'] }}</h3>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="card shadow" style="height: 104px;">
                                        <div class="card-body text-center">
                                                <h5 class="text-red font-weight-bold" style="text-decoration: underline;">Barang Stok Habis</h5>
                                                <h3 class="m-0">{{ $infoBarang['barang_habis'] }}</h3>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="card shadow" style="height: 104px;">
                                        <div class="card-body text-center">
                                                <h5 class="text-success font-weight-bold" style="text-decoration: underline;">Terlaris Minggu Ini</h5>
                                                <h4 class="m-0">Doxyvet 250 gram</h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                         <div class="table-responsive rounded mb-3">
                             <table class="data-table table mb-0 tbl-server-info table-hover">
                                 <thead class="bg-white text-uppercase">
                                     <tr class="ligth ligth-data">
                                         <th>No</th>
                                         <th>Nama Barang</th>
                                         <th>Keterangan</th>
                                         <th>Stok</th>
                                         <th>Harga Jual</th>
                                         <th>Harga DO</th>
                                         <th>Updated</th>
                                         <th>Action</th>
                                     </tr>
                                 </thead>
                                 <tbody class="ligth-body">
                                   @foreach($barang as $key => $b)
                                     <tr>
                                         <td>{{ $key+1 }}</td>
                                         <td>{{ $b->nama_barang }}</td>
                                         <td class="text-center">{{ $b->keterangan }}</td>
                                         <td class="text-center">
                                           @if($b->stok > 5)
                                            <b class="text-primary">{{ $b->stok }}</b>
                                           @else
                                            <b class="text-red">{{ $b->stok }}</b>
                                           @endif
                                         </td>
                                         <td>Rp{{ number_format($b->harga_jual) }}</td>
                                         <td>Rp{{ number_format($b->harga_do) }}</td>
                                         <td class="text-center">
                                            @if($b->updated_at != '')
                                              <small><i class="las la-calendar"></i> {{ date('d/m/Y', strtotime($b->updated_at)) }} </small>
                                            @else
                                              <b class="text-red">-</b>
                                            @endif
                                         </td>
                                         <td>
                                             <div class="d-flex align-items-center list-action">
                                                  <span data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit">
                                                    <a class="badge bg-primary mr-2" data-toggle="modal" href="#editBarang{{ $b->id_barang }}"><i class="ri-pencil-line mr-0"></i></a>
                                                  </span>
                                                  <span data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete">
                                                    <a class="badge bg-red mr-2" data-toggle="modal" href="#hapusBarang{{ $b->id_barang }}"><i class="ri-delete-bin-line mr-0"></i></a>
                                                  </span>
                                             </div>
                                         </td>
                                     </tr>
                                  @endforeach
                                 </tbody>
                             </table>
                             </div>
                        </div>
                    </div>
                   </div>
               </div>
           </div>

       </div>
       <!-- Page end  -->
   </div>
   <!-- Modal Edit -->
   <div class="modal fade" id="edit-note" tabindex="-1" role="dialog" aria-hidden="true">
       <div class="modal-dialog modal-dialog-centered" role="document">
           <div class="modal-content">
               <div class="modal-body">
                   <div class="popup text-left">
                       <div class="media align-items-top justify-content-between">
                           <h3 class="mb-3">Product</h3>
                           <div class="btn-cancel p-0" data-dismiss="modal"><i class="las la-times"></i></div>
                       </div>
                       <div class="content edit-notes">
                           <div class="card card-transparent card-block card-stretch event-note mb-0">
                               <div class="card-body px-0 bukmark">
                                   <div class="d-flex align-items-center justify-content-between pb-2 mb-3 border-bottom">
                                       <div class="quill-tool">
                                       </div>
                                   </div>
                                   <div id="quill-toolbar1">
                                       <p>Virtual Digital Marketing Course every week on Monday, Wednesday and Saturday.Virtual Digital Marketing Course every week on Monday</p>
                                   </div>
                               </div>
                               <div class="card-footer border-0">
                                   <div class="d-flex flex-wrap align-items-ceter justify-content-end">
                                       <div class="btn btn-primary mr-3" data-dismiss="modal">Cancel</div>
                                       <div class="btn btn-outline-primary" data-dismiss="modal">Save</div>
                                   </div>
                               </div>
                           </div>
                       </div>
                   </div>
               </div>
           </div>
       </div>
   </div>
</div>

<div class="modal fade" id="modalTambahBarang" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="modalTambahBarangLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title font-weight-bold text-uppercase text-primary" id="modalTambahBarangLabel"><i class="fa fa-box"></i> Tambah Barang Baru</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="alert alert-danger" id="divError" role="alert" style="border-radius: 0; border-left: 4px solid red; display:none;">
            <div class="iq-alert-icon">
               <i class="ri-information-line"></i>
            </div>
            <div class="iq-alert-text" id="errorMessage"></div>
        </div>
        <form id="formTambahBarang" action="{{ url('barang') }}" method="post">
          @csrf
          <div class="row">
            <div class="col-lg-6">
              <div class="form-group">
                <label for="kode_barang">Kode Barang</label>
                <input type="text" class="form-control" name="kode_barang" id="kode_barang" aria-describedby="kodeBarang">
                <small id="kodeBarang" class="form-text text-warning">Masukkan kode barang yang telah di scan dari barcode.</small>
              </div>
            </div>
            <div class="col-lg-6">
              <div class="form-group">
                <label for="nama_barang">Nama Barang</label>
                <input type="text" class="form-control" name="nama_barang" id="nama_barang">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-6">
              <div class="form-group">
                <label for="keterangan">Keterangan</label>
                <input type="text" class="form-control" name="keterangan" id="keterangan">
              </div>
            </div>
            <div class="col-lg-6">
              <div class="form-group">
                <label for="stok_barang">Stok Barang</label>
                <input type="number" class="form-control" name="stok_barang" id="stok_barang" placeholder="0">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-6">
              <div class="form-group">
                <label for="harga_jual">Harga Jual</label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text" id="harga-jual">Rp</span>
                  </div>
                  <input type="number" class="form-control" placeholder="0" name="harga_jual" id="harga_jual" aria-describedby="harga-jual">
                </div>
              </div>
            </div>
            <div class="col-lg-6">
              <div class="form-group">
                <label for="harga_do">Harga DO</label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text" id="harga-do">Rp</span>
                  </div>
                  <input type="number" class="form-control" placeholder="0" name="harga_do" id="harga_do" aria-describedby="harga-do">
                </div>
              </div>
            </div>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
        <button type="submit" id="btnTambahBarang" class="btn btn-primary">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>

@foreach($barang as $b)

<!-- Hapus Barang -->
<div class="modal fade" id="hapusBarang{{ $b->id_barang }}" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="hapusBarang{{ $b->id_barang }}Label" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title font-weight-bold text-red" id="hapusBarang{{ $b->id_barang }}Label"><i class="fa fa-box"></i> Hapus Data Barang</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body text-center">
            <img src="{{ url('assets/img/delete.png') }}" alt="..." width="150px" class="mb-3"><br>
            Apakah anda yakin akan menghapus data <br> barang <b class="text-uppercase">{{ $b->nama_barang }}</b>?
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
          <form action="{{ url('deleteBarang') }}" method="POST">
            @csrf
            <input type="hidden" name="id_barang" value="{{ $b->id_barang }}">
            <button type="submit" class="btn btn-red">Hapus</button>
          </form>
        </div>
      </div>
    </div>
</div>

<!-- Edit Barang -->
<div class="modal fade" id="editBarang{{ $b->id_barang }}" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="editBarang{{ $b->id_barang }}Label" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title font-weight-bold text-uppercase text-primary" id="editBarang{{ $b->id_barang }}Label"><i class="fa fa-edit"></i> Edit Data Barang</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="alert alert-danger" id="divError{{ $b->id_barang }}" role="alert" style="border-radius: 0; border-left: 4px solid red; display:none;">
            <div class="iq-alert-icon">
               <i class="ri-information-line"></i>
            </div>
            <div class="iq-alert-text" id="errorMessage{{ $b->id_barang }}"></div>
        </div>
        <form id="formEditBarang{{ $b->id_barang }}" action="{{ url('editBarang') }}" method="post">
          @csrf
          <input type="hidden" name="id_barang" id="id_barang{{ $b->id_barang }}" value="{{ $b->id_barang }}">
          <div class="row">
            <div class="col-lg-6">
              <div class="form-group">
                <label for="kode_barang{{ $b->id_barang }}">Kode Barang</label>
                <input type="text" class="form-control" name="kode_barang" id="kode_barang{{ $b->id_barang }}" value="{{ $b->kode }}" aria-describedby="kodeBarang">
                <small id="kodeBarang" class="form-text text-warning">Masukkan kode barang yang telah di scan dari barcode.</small>
              </div>
            </div>
            <div class="col-lg-6">
              <div class="form-group">
                <label for="nama_barang{{ $b->id_barang }}">Nama Barang</label>
                <input type="text" class="form-control" name="nama_barang" id="nama_barang{{ $b->id_barang }}" value="{{ $b->nama_barang }}">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-6">
              <div class="form-group">
                <label for="keterangan{{ $b->id_barang }}">Keterangan</label>
                <input type="text" class="form-control" name="keterangan" id="keterangan{{ $b->id_barang }}" value="{{ $b->keterangan }}">
              </div>
            </div>
            <div class="col-lg-6">
              <div class="form-group">
                <label for="stok_barang{{ $b->id_barang }}">Stok Barang</label>
                <input type="number" class="form-control" name="stok_barang" id="stok_barang{{ $b->id_barang }}" placeholder="0" value="{{ $b->stok }}">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-6">
              <div class="form-group">
                <label for="harga_jual{{ $b->id_barang }}">Harga Jual</label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text" id="harga-jual">Rp</span>
                  </div>
                  <input type="number" class="form-control" placeholder="0" name="harga_jual" id="harga_jual{{ $b->id_barang }}" aria-describedby="harga-jual" value="{{ $b->harga_jual }}">
                </div>
              </div>
            </div>
            <div class="col-lg-6">
              <div class="form-group">
                <label for="harga_do{{ $b->id_barang }}">Harga DO</label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text" id="harga-do">Rp</span>
                  </div>
                  <input type="number" class="form-control" placeholder="0" name="harga_do" id="harga_do{{ $b->id_barang }}" aria-describedby="harga-do" value="{{ $b->harga_do }}">
                </div>
              </div>
            </div>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
        <button type="submit" id="btnEditBarang{{ $b->id_barang }}" class="btn btn-primary">Update</button>
        </form>
      </div>
    </div>
  </div>
</div>

@endforeach

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>

<script>
    $(document).ready(function() {
        $('.data-table').DataTable({
            "ordering": false
        });
        @if(Session::has('message_delete'))
            Swal.fire({
                icon: 'success',
                title: 'Berhasil',
                text: 'Data barang berhasil dihapus!'
            })
        @endif
        @if(Session::has('message_edit'))
            Swal.fire({
                icon: 'success',
                title: 'Berhasil',
                text: 'Data barang berhasil diubah!'
            })
        @endif
    });
</script>

<script>
  $(document).ready(function(){

    $('#formTambahBarang').submit(function(e){
      var kode_barang = $('#kode_barang').val();
      var nama_barang = $('#nama_barang').val();
      var keterangan = $('#keterangan').val();
      var stok_barang = $('#stok_barang').val();
      var harga_jual = $('#harga_jual').val();
      var harga_do = $('#harga_do').val();
      var divError = $('#divError');
      var errorMessage = $('#errorMessage');

      if(!kode_barang){
        divError.show(200)
        errorMessage.html('Kode barang tidak boleh kosong!')
        $('#kode_barang').focus()
        return false
      }
      if(!nama_barang){
        divError.show(200)
        errorMessage.html('Nama barang tidak boleh kosong!')
        $('#nama_barang').focus()
        return false
      }
      if(!keterangan){
        divError.show(200)
        errorMessage.html('Keterangan tidak boleh kosong!')
        $('#keterangan').focus()
        return false
      }
      if(!stok_barang){
        divError.show(200)
        errorMessage.html('Stok barang tidak boleh kosong!')
        $('#stok_barang').focus()
        return false
      }
      if(stok_barang == 0){
        divError.show(200)
        errorMessage.html('Stok barang tidak boleh bernilai 0!')
        $('#stok_barang').focus()
        return false
      }
      if(!harga_jual){
        divError.show(200)
        errorMessage.html('Harga jual tidak boleh kosong!')
        $('#stok_barang').focus()
        return false
      }
      if(!harga_do){
        divError.show(200)
        errorMessage.html('Harga DO tidak boleh kosong!')
        $('#harga_do').focus()
        return false
      }

      var formData = new FormData($("#formTambahBarang")[0]);
      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });
      $.ajax({
          type: "POST",
          url: $('#formTambahBarang').attr('action'),
          data: formData,
          dataType:'json',
          processData: false,
          contentType: false,
          beforeSend:function(){
              $('#btnTambahBarang').prop("disabled", true)
              $('#btnTambahBarang').html("Harap tunggu ...")
          },
          success: function(data){
              if(!data.success){
                  divError.show(200)
                  errorMessage.html('Koneksi tidak stabil, coba kembali!')
                  $('#btnTambahBarang').prop("disabled", false)
                  $('#btnTambahBarang').html("Simpan")
              }else{
                  divError.hide(200)
                  Swal.fire({
                      icon: 'success',
                      title: 'Berhasil',
                      text: data.message
                  })
                  window.location.href = 'barang'
              }
          }
      })

      e.preventDefault()
    })

  })
</script>

@foreach($barang as $b)
<script>
    $('#formEditBarang'+{{ $b->id_barang }}).submit(function(e){
      var id_barang = $('#id_barang'+{{ $b->id_barang }}).val()
      var kode_barang = $('#kode_barang'+{{ $b->id_barang }}).val()
      var nama_barang = $('#nama_barang'+{{ $b->id_barang }}).val()
      var keterangan = $('#keterangan'+{{ $b->id_barang }}).val()
      var stok_barang = $('#stok_barang'+{{ $b->id_barang }}).val()
      var harga_jual = $('#harga_jual'+{{ $b->id_barang }}).val()
      var harga_do = $('#harga_do'+{{ $b->id_barang }}).val()
      var divError = $('#divError'+{{ $b->id_barang }})
      var errorMessage = $('#errorMessage'+{{ $b->id_barang }})

      if(!kode_barang){
        divError.show(200)
        errorMessage.html('Kode barang tidak boleh kosong!')
        $('#kode_barang'+{{ $b->id_barang }}).focus()
        return false
      }
      if(!nama_barang){
        divError.show(200)
        errorMessage.html('Nama barang tidak boleh kosong!')
        $('#nama_barang'+{{ $b->id_barang }}).focus()
        return false
      }
      if(!keterangan){
        divError.show(200)
        errorMessage.html('Keterangan tidak boleh kosong!')
        $('#keterangan'+{{ $b->id_barang }}).focus()
        return false
      }
      if(!stok_barang){
        divError.show(200)
        errorMessage.html('Stok barang tidak boleh kosong!')
        $('#stok_barang'+{{ $b->id_barang }}).focus()
        return false
      }
      if(stok_barang == 0){
        divError.show(200)
        errorMessage.html('Stok barang tidak boleh bernilai 0!')
        $('#stok_barang'+{{ $b->id_barang }}).focus()
        return false
      }
      if(!harga_jual){
        divError.show(200)
        errorMessage.html('Harga jual tidak boleh kosong!')
        $('#stok_barang'+{{ $b->id_barang }}).focus()
        return false
      }
      if(!harga_do){
        divError.show(200)
        errorMessage.html('Harga DO tidak boleh kosong!')
        $('#harga_do'+{{ $b->id_barang }}).focus()
        return false
      }

      var formData = new FormData($("#formEditBarang"+{{ $b->id_barang }})[0]);
      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });
      $.ajax({
          type: "POST",
          url: $('#formEditBarang'+{{ $b->id_barang }}).attr('action'),
          data: formData,
          dataType:'json',
          processData: false,
          contentType: false,
          beforeSend:function(){
              $('#btnEditBarang'+{{ $b->id_barang }}).prop("disabled", true)
              $('#btnEditBarang'+{{ $b->id_barang }}).html("Harap tunggu ...")
          },
          success: function(data){
              if(!data.success){
                  divError.show(200)
                  errorMessage.html('Koneksi tidak stabil, coba kembali!')
                  $('#btnEditBarang'+{{ $b->id_barang }}).prop("disabled", false)
                  $('#btnEditBarang'+{{ $b->id_barang }}).html("Simpan")
              }else{
                  divError.hide(200)
                  window.location.href = 'barang'
              }
          }
      })

      e.preventDefault()
    })
</script>
@endforeach

@endsection
