@extends('template')

@section('content')

<div class="content-page">
     <div class="container-fluid">
        <div class="row">
            {{-- <div class="col-lg-12">
                <div class="d-flex flex-wrap align-items-center justify-content-between mb-4">
                    <div>
                        <h4 class="mb-3">Customer List</h4>
                        <p class="mb-0">A customer dashboard lets you easily gather and visualize customer data from optimizing <br>
                         the customer experience, ensuring customer retention. </p>
                    </div>
                    <a href="page-add-customers.html" class="btn btn-primary add-list"><i class="las la-plus mr-3"></i>Add Customer</a>
                </div>
            </div> --}}
            <div class="col-lg-12">
                <div class="alert alert-light">
                    <b class="text-primary"><i class="fa fa-home"></i> Home </b> <span class="mx-1">/</span> Data User
                </div>
            </div>
            <div class="col-lg-12">
                <div class="row">
                    @php
                        $totAdmin = 0;
                        $totKasir = 0;
                        foreach($dataUser as $u){
                            if($u->role_id == 1){
                                $totAdmin += 1;
                            }else{
                                $totKasir += 1;
                            }
                        }
                    @endphp
                    <div class="col-lg-4">
                        <div class="card shadow" style="border-radius: 0;">
                            <div class="card-body text-center">
                                <h5 class="text-danger" style="text-decoration: underline;">Total User</h5>
                                <h3 class="m-0">{{ count($dataUser) }}</h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="card shadow" style="border-radius: 0;">
                            <div class="card-body text-center">
                                <h5 class="text-secondary" style="text-decoration: underline;">Jumlah Admin</h5>
                                <h3 class="m-0">{{ $totAdmin }}</h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="card shadow" style="border-radius: 0;">
                            <div class="card-body text-center">
                                <h5 class="text-success" style="text-decoration: underline;">Jumlah Kasir</h5>
                                <h3 class="m-0">{{ $totKasir }}</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="card bg-white shadow">
                    <div class="card-header py-3 font-weight-bold">
                        <div class="row">
                            <div class="col-lg-6 my-auto">
                                <h6 class="m-0 text-primary"><i class="fa fa-users"></i> DATA USER</h6>
                            </div>
                            <div class="col-lg-6 text-right">
                                <a href="#tambahUser" data-toggle="modal" class="btn btn-sm btn-primary add-list font-weight-bold"><i class="fa fa-plus-circle mr-2"></i>Tambah User</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="table-responsive rounded mb-3">
                                <table class="data-table table mb-0 tbl-server-info">
                                    <thead class="bg-white text-uppercase">
                                        <tr class="ligth ligth-data">
                                            <th>No</th>
                                            <th>Nama</th>
                                            <th class="text-center">Username</th>
                                            <th class="text-center">Role</th>
                                            <th class="text-center">Tgl Dibuat</th>
                                            <th class="text-center">Tgl Update</th>
                                            <th class="text-center">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody class="ligth-body">
                                        @foreach($dataUser as $no => $u)
                                        <tr>
                                            <td>{{ $no+1 }}</td>
                                            <td class="font-weight-bold">{{ $u->nama }}</td>
                                            <td class="text-center">{{ $u->username }}</td>
                                            <td class="text-center">
                                                @if($u->role_id == 1)
                                                    <div class="badge badge-warning">Admin</div>
                                                @else
                                                    <div class="badge badge-success">Kasir</div>
                                                @endif
                                            </td>
                                            <td class="text-dark text-center">
                                                <small><i class="fa fa-calendar"></i> {{ date('d/m/Y', strtotime($u->created_at)) }}</small>
                                            </td>
                                            <td class="text-primary text-center">
                                                @if($u->created_at == $u->updated_at)
                                                    <b class="text-danger">-</b>
                                                @else
                                                    <small><i class="fa fa-edit"></i> {{ date('d/m/Y', strtotime($u->updated_at)) }}</small>
                                                @endif
                                            </td>
                                            <td class="text-center">
                                                <div class="d-flex align-items-center list-action">
                                                    <span data-toggle="tooltip" data-placement="top" title="" data-original-title="View">
                                                        <a class="badge badge-info mr-2" href="#viewUser{{ $u->id_user }}" data-toggle="modal"><i class="ri-eye-line mr-0"></i></a>
                                                    </span>
                                                    <span data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit">
                                                        <a class="badge bg-success mr-2" href="#editUser{{ $u->id_user }}" data-toggle="modal"><i class="ri-pencil-line mr-0"></i></a>
                                                    </span>
                                                    <span data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete">
                                                        <a class="badge bg-warning mr-2" href="#deleteUser{{ $u->id_user }}" data-toggle="modal"><i class="ri-delete-bin-line mr-0"></i></a>
                                                    </span>
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- Page end  -->
    </div>

    <!-- Modal Edit -->
    <div class="modal fade" id="edit-note" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="popup text-left">
                        <div class="media align-items-top justify-content-between">
                            <h3 class="mb-3">Product</h3>
                            <div class="btn-cancel p-0" data-dismiss="modal"><i class="las la-times"></i></div>
                        </div>
                        <div class="content edit-notes">
                            <div class="card card-transparent card-block card-stretch event-note mb-0">
                                <div class="card-body px-0 bukmark">
                                    <div class="d-flex align-items-center justify-content-between pb-2 mb-3 border-bottom">
                                        <div class="quill-tool">
                                        </div>
                                    </div>
                                    <div id="quill-toolbar1">
                                        <p>Virtual Digital Marketing Course every week on Monday, Wednesday and Saturday.Virtual Digital Marketing Course every week on Monday</p>
                                    </div>
                                </div>
                                <div class="card-footer border-0">
                                    <div class="d-flex flex-wrap align-items-ceter justify-content-end">
                                        <div class="btn btn-primary mr-3" data-dismiss="modal">Cancel</div>
                                        <div class="btn btn-outline-primary" data-dismiss="modal">Save</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- View User Modal --}}
    @foreach($dataUser as $u)
    <div class="modal fade" id="viewUser{{ $u->id_user }}" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="viewUser{{ $u->id_user }}Label" aria-hidden="true">
        <div class="modal-dialog ">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title font-weight-bold text-secondary" id="viewUser{{ $u->id_user }}Label"><i class="fa fa-id-card"></i> Detail User</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <table class="table table-bordered" style="border-radius: 0;">
                    <tr>
                        <td colspan="2" class="font-weight-bold text-center">DETAIL USER</td>
                    </tr>
                    <tr>
                        <td class="bg-white font-weight-bold">Nama</td>
                        <td>{{ $u->nama }}</td>
                    </tr>
                    <tr>
                        <td class="bg-white font-weight-bold">Username</td>
                        <td>{{ $u->username }}</td>
                    </tr>
                    <tr>
                        <td class="bg-white font-weight-bold">Role</td>
                        <td>
                            @if($u->role_id == 1)
                                <span class="badge badge-secondary">Administrator</span>
                            @else
                                <span class="badge badge-success">Kasir</span>
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td class="bg-white font-weight-bold">Tgl Dibuat</td>
                        <td><i class="fa fa-calendar"></i> {{ date('d/m/Y H:i', strtotime($u->created_at)) }}</td>
                    </tr>
                    <tr>
                        <td class="bg-white font-weight-bold">Tgl Update</td>
                        <td class="text-primary"><i class="fa fa-edit"></i> {{ date('d/m/Y H:i', strtotime($u->updated_at)) }}</td>
                    </tr>
              </table>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
              {{-- <button type="button" class="btn btn-primary">Understood</button> --}}
            </div>
          </div>
        </div>
    </div>

    <div class="modal fade" id="deleteUser{{ $u->id_user }}" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="deleteUser{{ $u->id_user }}Label" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title font-weight-bold text-red" id="deleteUser{{ $u->id_user }}Label"><i class="fa fa-trash"></i> Hapus User</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body text-center">
                <img src="{{ url('assets/img/delete.png') }}" alt="..." width="150px" class="mb-3"><br>
                Apakah anda yakin akan menghapus data <br> user bernama <b>{{ $u->nama }}</b>?
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
              <form action="{{ url('deleteUser') }}" method="POST">
                @csrf
                <input type="hidden" name="id_user" value="{{ $u->id_user }}">
                <button type="submit" class="btn btn-red">Hapus</button>
              </form>
            </div>
          </div>
        </div>
    </div>

    {{-- Edit --}}
    <div class="modal fade" id="editUser{{ $u->id_user }}" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="editUser{{ $u->id_user }}Label" aria-hidden="true">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title text-primary font-weight-bold" id="editUser{{ $u->id_user }}Label"><i class="fa fa-user-edit"></i> Edit Data User</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <div class="alert alert-danger" id="divError{{ $u->id_user }}" role="alert" style="border-radius: 0; border-left: 4px solid red; display:none;">
                    <div class="iq-alert-icon">
                       <i class="ri-information-line"></i>
                    </div>
                    <div class="iq-alert-text" id="errorMessage{{ $u->id_user }}"></div>
                </div>
                <form id="formEditUser{{ $u->id_user }}" method="POST" action="{{ url('editUser') }}">
                    @csrf
                    <input type="hidden" id="id_user{{ $u->id_user }}" name="id_user" value="{{ $u->id_user }}">
                    <input type="hidden" id="username_old{{ $u->id_user }}" name="username_old" value="{{ $u->username }}">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                              <label for="nama{{ $u->id_user }}"><i class="fa fa-angle-right"></i> Nama</label>
                              <input type="text" class="form-control has-error" id="nama{{ $u->id_user }}" name="nama" value="{{ $u->nama }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="username{{ $u->id_user }}"><i class="fa fa-angle-right"></i> Username</label>
                                <input type="text" class="form-control" id="username{{ $u->id_user }}" name="username" value="{{ $u->username }}">
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="role{{ $u->id_user }}"><i class="fa fa-angle-right"></i> Role/Status</label>
                                <select class="form-control select" name="role" id="role{{ $u->id_user }}">
                                    <option value="">-- Pilih Role --</option>
                                    <option value="1" {{ ($u->role_id == 1) ? 'selected' : ''}}>Administrator</option>
                                    <option value="2" {{ ($u->role_id == 2) ? 'selected' : ''}}>Kasir</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <span class="btn btn-block btn-danger" style="border-radius: 0; background: red;"><b>NB*:</b> Kosongi bagian ini jika tidak mengubah password.</span>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="password{{ $u->id_user }}"><i class="fa fa-angle-right"></i> Password</label>
                                <input type="password" class="form-control" id="password{{ $u->id_user }}" name="password">
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="password_confirm{{ $u->id_user }}"><i class="fa fa-angle-right"></i> Konfirmasi Password</label>
                                <input type="password" class="form-control" id="password_confirm{{ $u->id_user }}" name="password_confirm">
                            </div>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
              <button type="submit" id="btnEditUser{{ $u->id_user }}" class="btn btn-primary">Edit</button>
            </form>
            </div>
          </div>
        </div>
    </div>

    @endforeach
    {{-- End View User Modal --}}

</div>

<div class="modal fade" id="tambahUser" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="tambahUserLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title text-primary" id="tambahUserLabel"><i class="fa fa-user-plus"></i> Tambah User Baru</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div class="alert alert-danger" id="divError" role="alert" style="border-radius: 0; border-left: 4px solid red; display:none;">
                <div class="iq-alert-icon">
                   <i class="ri-information-line"></i>
                </div>
                <div class="iq-alert-text" id="errorMessage"></div>
            </div>
            <form id="formTambahUser" method="POST" action="{{ url('user') }}">
                @csrf
                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group">
                          <label for="nama"><i class="fa fa-angle-right"></i> Nama</label>
                          <input type="text" class="form-control has-error" id="nama" name="nama">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="username"><i class="fa fa-angle-right"></i> Username</label>
                            <input type="text" class="form-control" id="username" name="username">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="role"><i class="fa fa-angle-right"></i> Role/Status</label>
                            <select class="form-control select" name="role" id="role">
                                <option value="">-- Pilih Role --</option>
                                <option value="1">Administrator</option>
                                <option value="2">Kasir</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="password"><i class="fa fa-angle-right"></i> Password</label>
                            <input type="password" class="form-control" id="password" name="password">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="password_confirm"><i class="fa fa-angle-right"></i> Konfirmasi Password</label>
                            <input type="password" class="form-control" id="password_confirm" name="password_confirm">
                        </div>
                    </div>
                </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
          <button type="submit" id="btnTambahUser" class="btn btn-primary">Simpan</button>
        </form>
        </div>
      </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>

<script>
    $(document).ready(function() {
        $('.data-table').DataTable({
            "ordering": false
        });
        @if(Session::has('message'))
            Swal.fire({
                icon: 'success',
                title: 'Berhasil',
                text: 'Data user berhasil dihapus'
            })
        @endif
    });
</script>

@foreach($dataUser as $u)
<script>
    $('#formEditUser'+{{ $u->id_user }}).submit(function(e){
        var id_user             = $('#id_user'+{{ $u->id_user }}).val()
        var nama                = $('#nama'+{{ $u->id_user }}).val()
        var username_old        = $('#username_old'+{{ $u->id_user }}).val()
        var username            = $('#username'+{{ $u->id_user }}).val()
        var role                = $('#role'+{{ $u->id_user }}).val()
        var password            = $('#password'+{{ $u->id_user }}).val()
        var password_confirm    = $('#password_confirm'+{{ $u->id_user }}).val()
        var divError            = $('#divError'+{{ $u->id_user }})
        var errorMessage        = $('#errorMessage'+{{ $u->id_user }})

        if(!nama){
            divError.show(200)
            errorMessage.html('<b>Nama</b> harus diisi!')
            $('#nama'+{{ $u->id_user }}).focus()
            return false
        }
        if(!username){
            divError.show(200)
            errorMessage.html('<b>Username</b> harus diisi!')
            $('#username'+{{ $u->id_user }}).focus()
            return false
        }
        if(!role){
            divError.show(200)
            errorMessage.html('<b>Role</b> harus dipilih!')
            $('#role'+{{ $u->id_user }}).focus()
            return false
        }
        if(password){
            if(password.length < 6){
                divError.show(200)
                errorMessage.html('<b>Password</b> minimal 6 karakter!')
                $('#password'+{{ $u->id_user }}).focus()
                return false
            }
            if(!password_confirm){
                divError.show(200)
                errorMessage.html('<b>Konfirmasi password</b> harus diisi!')
                $('#password_confirm'+{{ $u->id_user }}).focus()
                return false
            }
            if(password != password_confirm){
                divError.show(200)
                errorMessage.html('<b>Password</b> tidak sesuai!')
                $('#password_confirm'+{{ $u->id_user }}).focus()
                return false
            }
        }

        var formData = new FormData($("#formEditUser"+{{ $u->id_user }})[0]);
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "POST",
            url: $('#formEditUser'+{{ $u->id_user }}).attr('action'),
            data: formData,
            dataType:'json',
            processData: false,
            contentType: false,
            beforeSend:function(){
                $('#btnEditUser'+{{ $u->id_user }}).prop("disabled", true)
                $('#btnEditUser'+{{ $u->id_user }}).html("Harap tunggu ...")
            },
            success: function(data){
                if(!data.success){
                    divError.show(200);
                    errorMessage.html('<b>Username sudah ada,</b> gunakan yang lain!')
                    $('#btnEditUser'+{{ $u->id_user }}).prop("disabled", false)
                    $('#btnEditUser'+{{ $u->id_user }}).html("Edit")
                }else{
                    divError.hide(200);
                    Swal.fire({
                        icon: 'success',
                        title: 'Berhasil',
                        text: data.message
                    })
                    window.location.href = 'user'
                }
            }
        })

        e.preventDefault()
    })
</script>
@endforeach

@endsection
