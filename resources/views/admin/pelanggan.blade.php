@extends('template')

@section('content')

<div class="content-page">
<div class="container-fluid">
   <div class="row">
      <div class="col-sm-12">
         <div class="card bg-white shadow">
            <div class="card-header d-flex justify-content-between">
               <div class="header-title">
                  <h4 class="card-title text-uppercase">
                      <i class="las la-user"></i> Data Pelanggan
                  </h4>
               </div>
               <a href="#tambahPelanggan" data-toggle="modal" class="btn btn-secondary float-right"><i class="las la-plus mr-2"></i>Tambah Pelanggan</a>
            </div>
            <div class="card-body">
               <div class="table-responsive">
                  <table class="data-table table table-striped dataTable mt-4" role="grid"
                     aria-describedby="user-list-page-info">
                     <thead>
                        <tr class="ligth">
                           <th>No</th>
                           <th>Nama Pelanggan</th>
                           <th class="text-center">Alamat</th>
                           <th class="text-center">Tgl Dibuat</th>
                           <th class="text-center">Terakhir Diubah</th>
                           <th class="text-center">Action</th>
                        </tr>
                     </thead>
                     <tbody>
                        @foreach($pelanggan as $no => $p)
                        <tr>
                            <td>{{ $no+1 }}</td>
                            <td>{{ $p->nama_pelanggan }}</td>
                            <td class="text-center">{{ $p->alamat }}</td>
                            <td class="text-center">
                                <small>
                                    <i class="las la-calendar"></i> {{ date('d M Y', strtotime($p->created_at)) }}
                                </small>
                            </td>
                            <td class="text-center">
                              @if($p->updated_at != '')
                              <small class="text-primary">
                                <i class="fa fa-edit"></i> {{ date('d M Y', strtotime($p->updated_at)) }}
                              </small>
                              @else
                              <b class="text-danger">-</b>
                              @endif
                            </td>
                            <td class="text-center">
                                <a href="#editPelanggan{{$p->id_pelanggan}}" data-toggle="modal" class="btn btn-sm bg-primary" style="border-radius:0;">Edit</a>
                                <a href="#hapusPelanggan{{$p->id_pelanggan}}" data-toggle="modal" class="btn btn-sm bg-red" style="border-radius:0;">Hapus</a>
                            </td>
                        </tr>
                        @endforeach
                     </tbody>
                  </table>
               </div>

            </div>
         </div>
      </div>
   </div>
</div>
</div>


<div class="modal fade" id="tambahPelanggan" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="tambahPelangganLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title font-weight-bold text-primary" id="tambahPelangganLabel"><i class="fa fa-user-plus"></i> Tambah Pelanggan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="alert alert-danger" id="divError" role="alert" style="border-radius: 0; border-left: 4px solid red; display:none;">
            <div class="iq-alert-icon">
               <i class="ri-information-line"></i>
            </div>
            <div class="iq-alert-text" id="errorMessage"></div>
        </div>
        <form id="formTambahPelanggan" action="{{ url('pelanggan') }}" method="post">
          <div class="form-group">
            <label for="nama" class="font-weight-bold">Nama Pelanggan</label>
            <input type="text" class="form-control" id="nama" name="nama">
          </div>
          <div class="form-group">
            <label for="alamat" class="font-weight-bold">Alamat Pelanggan</label>
            <input type="text" class="form-control" id="alamat" name="alamat">
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
        <button type="submit" id="btnTambahPelanggan" class="btn btn-primary">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>

@foreach($pelanggan as $p)

<div class="modal fade" id="hapusPelanggan{{ $p->id_pelanggan }}" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="hapusPelanggan{{ $p->id_pelanggan }}Label" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title font-weight-bold text-red" id="hapusPelanggan{{ $p->id_pelanggan }}Label"><i class="fa fa-trash"></i> Hapus Pelanggan</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body text-center">
            <img src="{{ url('assets/img/delete.png') }}" alt="..." width="150px" class="mb-3"><br>
            Apakah anda yakin akan menghapus data <br> pelanggan bernama <b>{{ $p->nama_pelanggan }}</b>?
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
          <form action="{{ url('deletePelanggan') }}" method="POST">
            @csrf
            <input type="hidden" name="id_pelanggan" value="{{ $p->id_pelanggan }}">
            <button type="submit" class="btn btn-red">Hapus</button>
          </form>
        </div>
      </div>
    </div>
</div>

<div class="modal fade" id="editPelanggan{{ $p->id_pelanggan }}" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="editPelanggan{{ $p->id_pelanggan }}Label" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title font-weight-bold text-primary" id="editPelanggan{{ $p->id_pelanggan }}Label"><i class="fa fa-user-edit"></i> Edit Data Pelanggan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="alert alert-danger" id="divError{{ $p->id_pelanggan }}" role="alert" style="border-radius: 0; border-left: 4px solid red; display:none;">
            <div class="iq-alert-icon">
               <i class="ri-information-line"></i>
            </div>
            <div class="iq-alert-text" id="errorMessage{{ $p->id_pelanggan }}"></div>
        </div>
        <form id="formEditPelanggan{{ $p->id_pelanggan }}" action="{{ url('editPelanggan') }}" method="post">
          @csrf
          <input type="hidden" name="id_pelanggan" id="id_pelanggan{{ $p->id_pelanggan }}" value="{{ $p->id_pelanggan }}">
          <div class="form-group">
            <label for="nama" class="font-weight-bold">Nama Pelanggan</label>
            <input type="text" class="form-control" id="nama{{ $p->id_pelanggan }}" name="nama" value="{{$p->nama_pelanggan}}">
          </div>
          <div class="form-group">
            <label for="alamat" class="font-weight-bold">Alamat Pelanggan</label>
            <input type="text" class="form-control" id="alamat{{ $p->id_pelanggan }}" name="alamat" value="{{$p->alamat}}">
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
        <button type="submit" id="btnEditPelanggan{{ $p->id_pelanggan }}" class="btn btn-primary">Update</button>
        </form>
      </div>
    </div>
  </div>
</div>

@endforeach


<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>

<script>
    $(document).ready(function() {
        $('.data-table').DataTable({
            "paging": false,
            "ordering": false
        });
        @if(Session::has('message_delete'))
            Swal.fire({
                icon: 'success',
                title: 'Berhasil',
                text: 'Data pelanggan berhasil dihapus!'
            })
        @endif
        @if(Session::has('message_edit'))
            Swal.fire({
                icon: 'success',
                title: 'Berhasil',
                text: 'Data pelanggan berhasil diubah!'
            })
        @endif
    });
</script>

<script>
  $(document).ready(function(){
    $('#formTambahPelanggan').submit(function(e){
      var nama = $('#nama').val()
      var alamat = $('#alamat').val()
      var divError = $('#divError')
      var errorMessage = $('#errorMessage')

      if(!nama){
        divError.show(200)
        errorMessage.html('<b>Nama</b> tidak boleh kosong!')
        $('#nama').focus()
        return false
      }
      if(!alamat){
        divError.show(200)
        errorMessage.html('<b>Alamat</b> tidak boleh kosong!')
        $('#alamat').focus()
        return false
      }

      var formData = new FormData($("#formTambahPelanggan")[0]);
      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });
      $.ajax({
          type: "POST",
          url: $('#formTambahPelanggan').attr('action'),
          data: formData,
          dataType:'json',
          processData: false,
          contentType: false,
          beforeSend:function(){
              $('#btnTambahPelanggan').prop("disabled", true)
              $('#btnTambahPelanggan').html("Harap tunggu ...")
          },
          success: function(data){
              if(!data.success){
                  divError.show(200)
                  errorMessage.html('Koneksi tidak stabil, coba kembali!')
                  $('#btnTambahPelanggan').prop("disabled", false)
                  $('#btnTambahPelanggan').html("Simpan")
              }else{
                  divError.hide(200)
                  window.location.href = 'pelanggan'
              }
          }
      })

      e.preventDefault()
    })
  })
</script>

@foreach($pelanggan as $p)
<script>
  $('#formEditPelanggan'+{{$p->id_pelanggan}}).submit(function(e){
    var id_pelanggan = $('#id_pelanggan'+{{$p->id_pelanggan}}).val()
    var nama = $('#nama'+{{$p->id_pelanggan}}).val()
    var alamat = $('#alamat'+{{$p->id_pelanggan}}).val()
    var divError = $('#divError'+{{$p->id_pelanggan}})
    var errorMessage = $('#errorMessage'+{{$p->id_pelanggan}})

    if(!nama){
      divError.show(200)
      errorMessage.html('<b>Nama</b> tidak boleh kosong!')
      $('#nama'+{{$p->id_pelanggan}}).focus()
      return false
    }
    if(!alamat){
      divError.show(200)
      errorMessage.html('<b>Alamat</b> tidak boleh kosong!')
      $('#alamat'+{{$p->id_pelanggan}}).focus()
      return false
    }

    var formData = new FormData($("#formEditPelanggan"+{{$p->id_pelanggan}})[0]);
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: "POST",
        url: $('#formEditPelanggan'+{{$p->id_pelanggan}}).attr('action'),
        data: formData,
        dataType:'json',
        processData: false,
        contentType: false,
        beforeSend:function(){
            $('#btnEditPelanggan'+{{$p->id_pelanggan}}).prop("disabled", true)
            $('#btnEditPelanggan'+{{$p->id_pelanggan}}).html("Harap tunggu ...")
        },
        success: function(data){
            if(!data.success){
                divError.show(200)
                errorMessage.html('Koneksi tidak stabil, coba kembali!')
                $('#btnEditPelanggan'+{{$p->id_pelanggan}}).prop("disabled", false)
                $('#btnEditPelanggan'+{{$p->id_pelanggan}}).html("Simpan")
            }else{
                divError.hide(200)
                window.location.href = 'pelanggan'
            }
        }
    })

    e.preventDefault()
  })
</script>
@endforeach

@endsection
