
$(document).ready(function(){

    $('#formTambahUser').submit(function(e){
        
        var nama = $('#nama').val()
        var username = $('#username').val()
        var role = $('#role').val()
        var password = $('#password').val()
        var password_confirm = $('#password_confirm').val()
        var divError = $('#divError')
        var errorMessage = $('#errorMessage')

        if(!nama){
            divError.show(200)
            errorMessage.html('<b>Nama</b> harus diisi!')
            $('#nama').focus()
            return false
        }
        if(!username){
            divError.show(200)
            errorMessage.html('<b>Username</b> harus diisi!')
            $('#username').focus()
            return false
        }
        if(!role){
            divError.show(200)
            errorMessage.html('<b>Role</b> harus dipilih!')
            $('#role').focus()
            return false
        }
        if(!password){
            divError.show(200)
            errorMessage.html('<b>Password</b> harus diisi!')
            $('#password').focus()
            return false
        }
        if(password.length < 6){
            divError.show(200)
            errorMessage.html('<b>Password</b> minimal 6 karakter!')
            $('#password').focus()
            return false
        }
        if(!password_confirm){
            divError.show(200)
            errorMessage.html('<b>Konfirmasi Password</b> harus diisi!')
            $('#password_confirm').focus()
            return false
        }
        if(password != password_confirm){
            divError.show(200)
            errorMessage.html('<b>Password</b> tidak sesuai!')
            $('#password_confirm').focus()
            return false
        }

        var formData = new FormData($("#formTambahUser")[0]);
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "POST",
            url: $('#formTambahUser').attr('action'),
            data: formData,
            dataType:'json',
            processData: false,
            contentType: false,
            beforeSend:function(){
                $('#btnTambahUser').prop("disabled", true)
                $('#btnTambahUser').html("Harap tunggu ...")
            },
            success: function(data){
                if(!data.success){
                    divError.show(200)
                    errorMessage.html('<b>Username sudah ada, </b> gunakan yang lain!')
                    $('#btnTambahUser').prop("disabled", false)
                    $('#btnTambahUser').html("Simpan")
                }else{
                    divError.hide(200)
                    Swal.fire({
                        icon: 'success',
                        title: 'Berhasil',
                        text: data.message
                    })
                    window.location.href = 'user'
                }
            }
        })
        
        e.preventDefault()
    })
    
})
