$(document).ready(function(){
    $('#formLogin').submit(function(e){
        var username = $('#username').val()
        var password = $('#password').val()
        var alertError = $('#alertError')
        var errorMessage = $('#errorMessage')

        if(!username){
            toastr.error('Username harus diisi!', 'Gagal!')
            alertError.show(200)
            errorMessage.text('Username harus diisi!')
            return false
        }
        if(!password){
            toastr.error('Password harus diisi!', 'Gagal!')
            alertError.show(200)
            errorMessage.text('Password harus diisi!')
            return false
        }
        if(password.length < 6){
            toastr.error('Password minimal 6 karakter!', 'Gagal!')
            alertError.show(200)
            errorMessage.text('Password minimal 6 karakter!')
            return false
        }

        var formData = new FormData($("#formLogin")[0]);
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "POST",
            url: $('#formLogin').attr('action'),
            data: formData,
            dataType:'json',
            processData: false,
            contentType: false,
            beforeSend:function(){
                $('#btnLogin').prop("disabled", true)
                $('#btnLogin').html("Harap tunggu ...")
            },
            success: function(data){
                console.log(data)
                if(!data.success){
                    toastr.error('Username / Password salah!', 'Gagal!')
                    alertError.show(200)
                    errorMessage.text('Username / Password salah!')
                    $('#btnLogin').prop("disabled", false)
                    $('#btnLogin').html("MASUK")
                    return false
                }else{
                    var role = data.user.role_id
                    toastr.success('Login Berhasil', 'Success')
                    if(role == 1){
                        window.location.href = 'admin'
                    }else{
                        window.location.href = 'kasir'
                    }
                }
            }
        })

        e.preventDefault()
    })
})