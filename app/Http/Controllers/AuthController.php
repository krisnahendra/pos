<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    
    public function index(){
        // dd(session()->has('user'));
        if(!session()->has('user')){
            return view('login');
        }else{
            $user = session()->get('user');
            if($user['role_id'] == 1){
                return redirect('admin');
            }else{
                return redirect('kasir');
            }   
        }
    }

    public function register(){
        return view('register');
    }

    public function postRegister(Request $req){
        date_default_timezone_set('Asia/Jakarta');
        User::create([
            'nama' => $req->nama,
            'username' => $req->username,
            'password' => Hash::make($req->password),
            'image' => 'default.jpg',
            'role_id' => 1,
            'aktif' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);
        return redirect('login');
    }

    public function checkLogin(Request $req){
        if(!auth()->attempt(['username' => $req->username, 'password' => $req->password])){
            return response()->json([
                'success' => false,
                'message' => 'Username / Password yang anda masukkan salah!'
            ]);
        }else{
            $user = Auth::user();
            $data = [
                'logged_in' => true,
                'id_user' => $user->id_user,
                'username' => $user->username,
                'nama' => $user->nama,
                'role_id' => $user->role_id
            ];
            session()->put('user', $data);
            return response()->json([
                'success' => true,
                'message' => 'Login Berhasil',
                'user' => $user
            ]);
        }
    }

    public function logout()
    {
        auth()->logout();
        session()->forget('user');
        session()->flash('message','You Have Been Logged Out');
        return redirect('login');
    }

}
