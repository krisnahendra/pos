<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use PDO;

class AdminController extends Controller
{

    public function index(){
        $loggedIn = session()->has('user');
        if($loggedIn){
            $data['title'] = 'Dashboard';
            $data['active'] = 'dashboard';
            $data['content'] = 'template';
            $data['user'] = session()->get('user');
            return view('admin/dashboard', $data);
        }
        return redirect('login');
    }

    public function user(){
        $loggedIn = session()->has('user');
        if($loggedIn){
            $data['title'] = 'Data User';
            $data['active'] = 'user';
            $data['content'] = 'template';
            $data['user'] = session()->get('user');
            $data['dataUser'] = User::orderby('role_id')->get();
            return view('admin/user', $data);
        }
        return redirect('login');
    }

    public function addUser(Request $req){
        $username = $req->username;
        $cekUsername = count(User::where('username', $username)->get());
        if($cekUsername == 1){
            return response()->json([
                'success' => false,
                'message' => 'Username sudah ada, gunakan yang lain!'
            ]);
        }else{
            User::create([
                'nama' => $req->nama,
                'username' => $req->username,
                'password' => Hash::make($req->password),
                'image' => 'default.jpg',
                'role_id' => $req->role,
                'aktif' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ]);
            return response()->json([
                'success' => true,
                'message' => 'Data user berhasil ditambahkan!'
            ]);
        }
    }

    public function deleteUser(Request $req){
        $id_user = $req->id_user;
        DB::table('users')->where('id_user', $id_user)->delete();
        session()->flash('message', 'Data user telah dihapus!');
        return redirect('user');
    }

    public function editUser(Request $req){
        date_default_timezone_set('Asia/Jakarta');
        if($req->username != $req->username_old){
            $cekUsername = count(User::where('username', $req->username)->get());
        }else{
            $cekUsername = 0;
        }

        if($cekUsername == 1){
            return response()->json([
                'success' => false,
                'message' => 'Username sudah ada, gunakan yang lain!'
            ]);
        }else{
            if($req->password == ''){
                User::where('id_user', $req->id_user)->update([
                    'nama' => $req->nama,
                    'username' => $req->username,
                    'role_id' => $req->role,
                    'updated_at' => date('Y-m-d H:i:s')
                ]);
            }else{
                User::where('id_user', $req->id_user)->update([
                    'nama' => $req->nama,
                    'username' => $req->username,
                    'password' => Hash::make($req->username),
                    'role_id' => $req->role,
                    'updated_at' => date('Y-m-d H:i:s')
                ]);
            }
            return response()->json([
                'success' => true,
                'message' => 'Data user berhasil diubah!'
            ]);
        }
    }

    public function barang(){
        $loggedIn = session()->has('user');
        if($loggedIn){
            $data['title'] = 'Data Barang';
            $data['active'] = 'barang';
            $data['content'] = 'template';
            $data['user'] = session()->get('user');
            $data['barang'] = DB::select("SELECT * FROM barang");
            $data['infoBarang'] = $this->infoBarang();
            return view('admin/barang', $data);
        }
        return redirect('login');
    }

    public function infoBarang(){
      $totBarang    = DB::select("SELECT COUNT(*) as total FROM barang");
      $barangHabis  = DB::select("SELECT COUNT(*) as total FROM barang WHERE stok = 0");
      $data = [
        'total_barang' => $totBarang[0]->total,
        'barang_habis' => $barangHabis[0]->total
      ];
      return $data;
    }

    public function tambahBarang(Request $req){
        date_default_timezone_set('Asia/Jakarta');
        $query = DB::table('barang')->insert([
                    'kode' => $req->kode_barang,
                    'nama_barang' => $req->nama_barang,
                    'keterangan' => $req->keterangan,
                    'harga_jual' => $req->harga_jual,
                    'harga_do' => $req->harga_do,
                    'stok' => $req->stok_barang,
                    'created_at' => date('Y-m-d H:i:s')
                  ]);
        if($query){
          return response()->json([
            'success' => true,
            'message' => 'Data barang baru berhasil disimpan!'
          ]);
        }
    }

    public function deleteBarang(Request $req){
        $id_barang = $req->id_barang;
        DB::table('barang')->where('id_barang', $id_barang)->delete();
        session()->flash('message_delete', 'Data barang telah dihapus!');
        return redirect('barang');
    }

    public function editBarang(Request $req){
        date_default_timezone_set('Asia/Jakarta');
        DB::table('barang')->where('id_barang', $req->id_barang)->update([
          'kode' => $req->kode_barang,
          'nama_barang' => $req->nama_barang,
          'keterangan' => $req->keterangan,
          'harga_jual' => $req->harga_jual,
          'harga_do' => $req->harga_do,
          'stok' => $req->stok_barang,
          'updated_at' => date('Y-m-d H:i:s')
        ]);
        session()->flash('message_edit', 'Data barang telah diubah!');
        return response()->json([
          'success' => true,
          'message' => 'Data barang berhasil diubah!'
        ]);
        // return redirect('barang');
    }

    public function pelanggan(){
        $loggedIn = session()->has('user');
        if($loggedIn){
            $data['title'] = 'Data Pelanggan';
            $data['active'] = 'pelanggan';
            $data['content'] = 'template';
            $data['user'] = session()->get('user');
            $data['pelanggan'] = DB::select("SELECT * FROM pelanggan");
            return view('admin/pelanggan', $data);
        }
        return redirect('login');
    }

    public function tambahPelanggan(Request $req){
        date_default_timezone_set('Asia/Jakarta');
        $query = DB::table('pelanggan')->insert([
          'nama_pelanggan' => $req->nama,
          'alamat' => $req->alamat,
          'created_at' => date('Y-m-d H:i:s')
        ]);
        if($query){
          return response()->json([
            'success' => true,
            'message' => 'Data pelanggan baru berhasil ditambahkan!'
          ]);
        }else{
          return response()->json([
            'success' => false,
            'message' => 'Gagal, koneksi tidak stabil, silahkan coba lagi!'
          ]);
        }
    }

    public function deletePelanggan(Request $req){
      $id_pelanggan = $req->id_pelanggan;
      DB::table('pelanggan')->where('id_pelanggan', $id_pelanggan)->delete();
      session()->flash('message_delete', 'Data pelanggan telah dihapus!');
      return redirect('pelanggan');
    }

    public function editPelanggan(Request $req){
      date_default_timezone_set('Asia/Jakarta');
      $query = DB::table('pelanggan')->where('id_pelanggan', $req->id_pelanggan)->update([
        'nama_pelanggan' => $req->nama,
        'alamat' => $req->alamat,
        'updated_at' => date('Y-m-d H:i:s')
      ]);
      if($query){
        session()->flash('message_edit', 'Data pelanggan berhasil diubah!');
        return response()->json([
          'success' => true,
          'message' => 'Data pelanggan berhasil diubah!'
        ]);
      }else{
        return response()->json([
          'success' => false,
          'message' => 'Gagal, koneksi tidak stabil, silahkan coba lagi!'
        ]);
      }
    }

    public function transaksi(Request $req){
        date_default_timezone_set('Asia/Jakarta');
        $loggedIn = session()->has('user');
        if($loggedIn){
            $data['title'] = 'Transaksi';
            $data['active'] = 'transaksi';
            $data['content'] = 'template';
            $data['user'] = session()->get('user');
            $data['cart'] = DB::select('SELECT * FROM cart c JOIN barang b ON b.kode = c.kode');
            $data['jenis'] = $req->get('jenis');
            return view('admin/transaksi', $data);
        }
        return redirect('login');
    }

    public function jenisTransaksi(Request $req){
      $cart = DB::select("SELECT * FROM cart c JOIN barang b ON b.kode = c.kode");
      $jenis = $req->get('jenis');
      if(count($cart) != 0){
        if($jenis == 'do'){
          foreach($cart as $c){
            DB::table('cart')->where('id', $c->id)->update([
              'harga' => $c->harga_do,
              'subtotal' => ($c->harga_do) * ($c->jumlah),
              'updated_at' => date('Y-m-d H:i:s')
            ]);
          }
        }else{
          foreach($cart as $c){
            DB::table('cart')->where('id', $c->id)->update([
              'harga' => $c->harga_jual,
              'subtotal' => ($c->harga_jual) * ($c->jumlah),
              'updated_at' => date('Y-m-d H:i:s')
            ]);
          }
        }
      }
      return redirect('transaksi?jenis='.$jenis);
    }

    public function jenisTransaksiBarcode(Request $req){
      $cart = DB::select("SELECT * FROM cart_barcode c JOIN barang b ON b.kode COLLATE utf8mb4_general_ci = c.kode");
      $jenis = $req->get('jenis');
      if(count($cart) != 0){
        if($jenis == 'do'){
          foreach($cart as $c){
            DB::table('cart_barcode')->where('id', $c->id)->update([
              'harga' => $c->harga_do,
              'subtotal' => ($c->harga_do) * ($c->jumlah),
              'updated_at' => date('Y-m-d H:i:s')
            ]);
          }
        }else{
          foreach($cart as $c){
            DB::table('cart_barcode')->where('id', $c->id)->update([
              'harga' => $c->harga_jual,
              'subtotal' => ($c->harga_jual) * ($c->jumlah),
              'updated_at' => date('Y-m-d H:i:s')
            ]);
          }
        }
      }
      return redirect('transaksiBarcode?jenis='.$jenis);
    }

    public function transaksiBarcode(Request $req){
        $loggedIn = session()->has('user');
        if($loggedIn){
            $data['title'] = 'Transaksi Barcode';
            $data['active'] = 'transaksi_barcode';
            $data['content'] = 'template';
            $data['user'] = session()->get('user');
            $data['cart'] = DB::select("SELECT * FROM cart_barcode JOIN barang ON barang.kode COLLATE utf8mb4_general_ci = cart_barcode.kode");
            $data['jenis'] = $req->get('jenis');
            return view('admin/transaksiBarcode', $data);
        }
        return redirect('login');
    }

    public function cariBarang(Request $req){
        // dd($req->get('jenisTransaksi'));
        $inputValue = $req->get('inputValue');
        $jenisTransaksi = $req->get('jenisTransaksi');

        if($inputValue != null){
          $barang = DB::select("SELECT * FROM barang WHERE nama_barang LIKE '%$inputValue%'");
          $listData = [];
          foreach($barang as $b){
            $listData[$b->id_barang]['id_barang'] = $b->id_barang;
            $listData[$b->id_barang]['kode'] = $b->kode;
            $listData[$b->id_barang]['nama_barang'] = $b->nama_barang;
            $listData[$b->id_barang]['keterangan'] = $b->keterangan;
          }

          $data = [];
          foreach($listData as $l){
            $data[] = '<li class="li-result" onclick="location.href=`addToCart?kode='.$l['kode'].'&jenisTransaksi='.$jenisTransaksi.'`">
                        <div class="row">
                          <div class="col-8">
                            <b>'.$l['nama_barang'].'</b> ('.$l['keterangan'].')
                          </div>
                          <div class="col-4 text-right">
                            <a href="addToCart?kode='.$l['kode'].'&jenisTransaksi='.$jenisTransaksi.'" class="btn btn-sm btn-dark font-weight-bold text-white">Tambah</a>
                          <div>
                        </div>
                      </li>';
          }

          $results = implode(" ", $data);
        }else{
          $results = '';
        }

        return response()->json([
          'data' => $results
        ]);
    }

    public function addToCart(Request $req){
      date_default_timezone_set('Asia/Jakarta');
      $kode_barang = $req->get('kode');
      $jenisTransaksi = $req->get('jenisTransaksi');
      $cekBarang = DB::select("SELECT COUNT(*) as jml FROM cart WHERE kode = '$kode_barang'");
      if($cekBarang[0]->jml == 0){
        $barang = DB::select("SELECT * FROM barang WHERE kode = '$kode_barang'");
        $count = DB::select("SELECT id FROM cart ORDER BY id DESC LIMIT 1");
        if(count($count) == 0){
          $hitungCount = 0;
        }else{
          $hitungCount = $count[0]->id;
        }

        DB::table('cart')->insert([
          'id' => $hitungCount + 1,
          'kode' => $barang[0]->kode,
          'harga' => $barang[0]->harga_jual,
          'jumlah' => 1,
          'subtotal' => $barang[0]->harga_jual,
          'created_at' => date('Y-m-d H:i:s')
        ]);
      }else{
        session()->flash('barangSudahAda', 'Barang sudah ada dalam keranjang!');
      }
      return redirect('transaksi?jenis='.$jenisTransaksi);
    }

    public function addToCartBarcode(Request $req){
      date_default_timezone_set('Asia/Jakarta');
      $kode_barang = $req->kode_barang;
      $jenisTransaksi = $req->jenisTransaksi;
      $cekBarang = DB::select("SELECT COUNT(*) as jml FROM cart_barcode WHERE kode = '$kode_barang'");
      if($cekBarang[0]->jml == 0){
        $barang = DB::select("SELECT * FROM barang WHERE kode = '$kode_barang'");
        $count = DB::select("SELECT id FROM cart_barcode ORDER BY id DESC LIMIT 1");
        if(count($count) == 0){
          $hitungCount = 0;
        }else{
          $hitungCount = $count[0]->id;
        }

        DB::table('cart_barcode')->insert([
          'id' => $hitungCount + 1,
          'kode' => $barang[0]->kode,
          'harga' => $barang[0]->harga_jual,
          'jumlah' => 1,
          'subtotal' => $barang[0]->harga_jual,
          'created_at' => date('Y-m-d H:i:s')
        ]);
      }else{
        session()->flash('barangSudahAda', 'Barang sudah ada dalam keranjang!');
      }
      return redirect('transaksiBarcode?jenis='.$jenisTransaksi);
    }

    public function deleteCart(Request $req){
      $id_cart = $req->get('id_cart');
      $jenisTransaksi = $req->get('jenis');
      DB::table('cart')->where('id', $id_cart)->delete();
      return redirect('transaksi?jenis='.$jenisTransaksi);
    }

    public function deleteCartBarcode(Request $req){
      $id_cart = $req->get('id_cart');
      $jenisTransaksi = $req->get('jenis');
      DB::table('cart_barcode')->where('id', $id_cart)->delete();
      return redirect('transaksiBarcode?jenis='.$jenisTransaksi);
    }

    public function clearCart(Request $req){
      $jenisTransaksi = $req->get('jenis');
      DB::select('TRUNCATE cart');
      return redirect('transaksi?jenis='.$jenisTransaksi);
    }

    public function clearCartBarcode(Request $req){
      $jenisTransaksi = $req->get('jenis');
      DB::select('TRUNCATE cart_barcode');
      return redirect('transaksiBarcode?jenis='.$jenisTransaksi);
    }

    public function submitCart(Request $req){
      date_default_timezone_set('Asia/Jakarta');
      $jenisTransaksi = $req->jenisTransaksi;

      $cekStok = [];
      foreach($req->id_cart as $k => $v){
        $barang = DB::select("SELECT * FROM cart WHERE id = '$v'");
        $kodeBarang = $barang[0]->kode;
        $stokBarang = DB::select("SELECT * FROM barang WHERE kode = '$kodeBarang'");
        $cekStok[$v] = ($stokBarang[0]->stok >= $req->jumlah[$k]);
      }

      $statusStok = in_array(false, $cekStok);

      if(!$statusStok){
        if($req->update != ''){
          // Update
          foreach($req->id_cart as $key => $val){
            $dataBarang = DB::select("SELECT * FROM cart WHERE id = '$val'");
            $hargaBarang = $dataBarang[0]->harga;

            DB::table('cart')->where('id', $val)->update([
              'jumlah' => $req->jumlah[$key],
              'subtotal' => $req->jumlah[$key] * $hargaBarang,
              'updated_at' => date('Y-m-d H:i:s')
            ]);
          }
          return redirect('transaksi?jenis='.$jenisTransaksi);
        }else{
          // Print
          $transaksi1 = DB::table('transaksi')->insert([
                          'kode' => $req->kode_transaksi,
                          'nama_kasir' => $req->kasir,
                          'nama_pembeli' => $req->pelanggan,
                          'total' => $req->grand_total,
                          'created_at' => date('Y-m-d H:i:s')
                        ]);
          $countTransaksi = DB::select("SELECT id_transaksi FROM transaksi ORDER BY id_transaksi DESC LIMIT 1");
          $foreignIdTransaksi = $countTransaksi[0]->id_transaksi;
          foreach($req->id_cart as $c){
            $getCart = DB::select("SELECT * FROM cart c JOIN barang b ON b.kode = c.kode WHERE id = '$c'");
            DB::table('detail_transaksi')->insert([
              'id_transaksi' => $foreignIdTransaksi,
              'id_barang' => $getCart[0]->id_barang,
              'jumlah' => $getCart[0]->jumlah,
              'created_at' => date('Y-m-d H:i:s')
            ]);
          }
          // Clear
          DB::select("TRUNCATE cart");
          return redirect('printStruk');
        }
      }else{
        session()->flash('stokHabis', 'Stok barang tidak mencukupi!');
        return redirect('transaksi?jenis='.$jenisTransaksi);
      }
    }


    public function riwayatTransaksi(){
      $loggedIn = session()->has('user');
      if($loggedIn){
          $data['title'] = 'Riwayat Transaksi';
          $data['active'] = 'riwayat_transaksi';
          $data['content'] = 'template';
          $data['user'] = session()->get('user');
          return view('admin/riwayat_transaksi', $data);
      }
      return redirect('login');
    }

    public function printStruk(){
      $lastTransaksi = DB::select("SELECT * FROM transaksi ORDER BY created_at DESC LIMIT 1");
      $lastIdTransaksi = $lastTransaksi[0]->id_transaksi;
      $data['transaksi'] = DB::select("SELECT * FROM detail_transaksi d JOIN barang b ON b.id_barang = d.id_barang WHERE d.id_transaksi = '$lastIdTransaksi'");
      $data['no_struk'] = $lastTransaksi[0]->kode;
      $data['nama_kasir'] = $lastTransaksi[0]->nama_kasir;
      $data['pelanggan'] = $lastTransaksi[0]->nama_pembeli;
      $data['total_all'] = $lastTransaksi[0]->total;
      // echo "<pre>";
      // print_r($data['transaksi']);die;
      return view('admin/printStruk', $data);
    }

}
